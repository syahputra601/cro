-- phpMyAdmin SQL Dump
-- version 5.0.2
-- https://www.phpmyadmin.net/
--
-- Host: localhost
-- Waktu pembuatan: 07 Des 2021 pada 04.55
-- Versi server: 10.4.11-MariaDB
-- Versi PHP: 7.4.6

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `db_cro`
--

-- --------------------------------------------------------

--
-- Struktur dari tabel `mst_company`
--

CREATE TABLE `mst_company` (
  `nama` varchar(100) NOT NULL,
  `alamat` varchar(500) NOT NULL,
  `kota` varchar(100) NOT NULL,
  `telp` varchar(25) NOT NULL,
  `divisi` varchar(100) NOT NULL,
  `fax` varchar(25) NOT NULL,
  `ppn` int(11) NOT NULL,
  `id_company` int(11) NOT NULL,
  `id_parent` int(11) NOT NULL,
  `kode_kantor` varchar(50) NOT NULL,
  `kode_company` varchar(50) NOT NULL,
  `email` varchar(50) NOT NULL,
  `website` varchar(200) NOT NULL,
  `id_coa_rak_aktiva` int(11) NOT NULL,
  `id_coa_rak_pasiva` int(11) NOT NULL,
  `logo` varchar(500) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `mst_company`
--

INSERT INTO `mst_company` (`nama`, `alamat`, `kota`, `telp`, `divisi`, `fax`, `ppn`, `id_company`, `id_parent`, `kode_kantor`, `kode_company`, `email`, `website`, `id_coa_rak_aktiva`, `id_coa_rak_pasiva`, `logo`) VALUES
('PT. SUPER SUKSES MOTOR', 'RUKAN KIRANA BOUTIQUE OFFICE BLOK D2 NO. 5 - 6 JL. BOULEVARD RAYA NO. 1 KELAPA GADING TIMUR JAKARTA UTARA', 'Jakarta', '021-29375657', '', '021-29375658', 0, 1, 0, 'HO', 'SSMHO', '', '', 0, 0, ''),
('SSM 3S IMAM BONJOL', 'Jl. Imam Bonjol No. 44,  RT 010, Pelabuhan Samarinda Ilir', 'Samarinda', '', '', '', 0, 23, 4, 'KALTIM', 'SSMIMB', '', '', 0, 0, ''),
('SSM 5S KARANG JATI', 'Jl. A. Yani RT.022, Karang Jati (Depan Gapura Karang Jawa) Balikpapan', 'Balikpapan', '0542 - 771606', '', '', 0, 24, 4, 'KALTIM', 'SSMKRJ', '', '', 0, 0, ''),
('SSM AREA JAKARTA', 'JL RS FATMAWATI RAYA NO. 108 A, GANDARIA SELATAN CILANDAK	 JAKARTA SELATAN', 'Jakarta', '021 - 75912429 / 75912431', '', '', 0, 2, 1, 'JKT', 'SSMJKT', '', '', 0, 0, ''),
('SSM AREA KALBAR', 'JL GAJAHMADA NO 149 A-B PONTIANAK', 'Pontianak', '0561-762669', '', '0561-762670', 0, 3, 1, 'KALBAR', 'SSMKALBAR', '', '', 0, 0, ''),
('SSM AREA KALSEL', 'JL A YANI KM 4.5 NO 2 BANJARMASIN', 'Banjarmasin', '0511-3260622 / 0821522449', '', '0511 - 3362481', 0, 8, 1, 'KALSEL', 'SSMKALSEL', '', '', 0, 0, ''),
('SSM AREA KALTIM', 'JL JEND A YANI RT 22 (DEPAN GAPURA KARANG JAWA), KARANG JATI - BALIKPAPAN', 'Balikpapan', '0542-415198', '', '', 0, 4, 1, 'KALTIM', 'SSMKALTIM', '', '', 0, 0, ''),
('SSM AREA SUMSEL', 'PALEMBANG', 'PALEMBANG', '', '', '', 0, 33, 1, 'SUMSEL', 'SSMSUMSEL', '', '', 0, 0, ''),
('SSM AYANI', 'JL.AHMAD YANI KM.4,5 RT. 02, BANJARMASIN', 'Banjarmasin', '0511 - 7404466', '', '', 0, 9, 8, 'KALSEL', 'SSMAYN', '', '', 0, 0, ''),
('SSM BALIKPAPAN PERMAI', 'Jl. Jend. Sudirman Kompleks A 90-91 Balikpapan Permai', 'Balikpapan', '0543 - 23286', '', '', 0, 21, 4, 'KALTIM', 'SSMBPP', '', '', 0, 0, ''),
('SSM BARONG TONGKOK', 'DESA SIMPANG RAYA, KAB KUTAI BARAT', 'BARONG TONGKOK', '', '', '', 0, 31, 4, 'KALTIM', 'SSMBTK', '', '', 0, 0, ''),
('SSM BASUKI RAHMAT', 'JALAN BASUKI RAHMAT NO 886', 'PALEMBANG', '', '', '', 0, 34, 33, 'SUMSEL', 'SSMBSR', '', '', 0, 0, ''),
('SSM BATURAJA', 'JL DR MOHAMMAD HATTA, DESA SUKARAYA KEC BATURAJA TIMUR', 'PALEMBANG', '', '', '', 0, 36, 33, 'SUMSEL', 'SSMBTR', '', '', 0, 0, ''),
('SSM BERAU', 'JL. S. A. MAULANA NO. 12, RT 16, TANJUNG REDEB, BERAU', 'Berau', '', '', '', 0, 7, 4, 'KALTIM', 'SSMBRU', '', '', 0, 0, ''),
('SSM BOGOR', 'JL RAYA TAJUR 39-D RT 004 / RW 001 TAJUR BOGOR TIMUR', 'BOGOR', '', '', '', 0, 38, 2, 'JKT', 'SSMBGR', '', '', 0, 0, ''),
('SSM BONTANG', 'Jl. S. Parman No.10, Km. 06, Bontang', 'Bontang', '0548 - 23897', '', '', 0, 22, 4, 'KALTIM', 'SSMBTG', '', '', 0, 0, ''),
('SSM BOSCH', 'JL RS FATMAWATI RAYA NO. 108 A, GANDARIA SELATAN CILANDAK	 JAKARTA SELATAN', 'JAKARTA', '021-54314536', '', '', 0, 39, 1, 'BOSCH', 'SSMBOSCH', '', '', 0, 0, ''),
('SSM DEPOK', 'JL. BOULEVARD NO.8 KP. PARUNG SERAB SUKMAJAYA DEPOK (500 M DARI GERBANG MASUK GRAND DEPOK CITY)', 'Depok', '021-770 2833', '', '', 0, 6, 2, 'JKT', 'SSMDPK', '', '', 0, 0, ''),
('SSM FATMAWATI', 'JL RS FATMAWATI RAYA NO. 108 A, GANDARIA SELATAN CILANDAK	 JAKARTA SELATAN', 'Jakarta', '021-75912429 / 52', '', '', 0, 5, 2, 'JKT', 'SSMFMT', '', '', 0, 0, ''),
('SSM KM12', 'JALAN NASIONAL LINTAS SUMATERA KM 12', 'PALEMBANG', '', '', '', 0, 35, 33, 'SUMSEL', 'SSMSMB', '', '', 0, 0, ''),
('SSM MARTAPURA', 'JL. A. YANI KM.37 RT.19, SUNGAI PARING, MARTAPURA, BANJAR 70613', 'Banjarmasin', '0511 - 4782138', '', '', 0, 12, 8, 'KALSEL', 'SSMMTP', '', '', 0, 0, ''),
('SSM PALANGKARAYA', 'Jl. RTA Nilono Km 3.5 Kec. Pahadut Kel. Langkai Palangkaraya - Kalimantan Tengah', 'Palangkaraya', '0536-4200141', '', '', 0, 15, 8, 'KALSEL', 'SSMPLK', '', '', 0, 0, ''),
('SSM PANGKALANBUN', 'JL. HM Rafi\'i , PANGKALANBUN', 'PANGKALANBUN', '', '', '', 0, 32, 8, 'KALSEL', 'SSMPKB', '', '', 0, 0, ''),
('SSM PCO', 'JL RS FATMAWATI RAYA NO. 108 A, GANDARIA SELATAN CILANDAK	 JAKARTA SELATAN', 'JAKARTA', '021-54314536', '', '', 0, 30, 1, 'PCO', 'SSMPCO', '', '', 0, 0, ''),
('SSM PCO 2', 'JL RS FATMAWATI RAYA NO. 108 A, GANDARIA SELATAN CILANDAK	 JAKARTA SELATAN', 'JAKARTA', '021-54314536', '', '', 0, 37, 30, 'PCO', 'SSMPCO2', '', '', 0, 0, ''),
('SSM PELAIHARI', 'JL. BOEJASIN KOMP. BARATA NO. 18, RT. 22 PELAIHARI KAB. TANAH LAUT KALIMANTAN SELATAN', 'Tanah Laut', '', '', '', 0, 14, 8, 'KALSEL', 'SSMPLH', '', '', 0, 0, ''),
('SSM PEMUDA', 'Jl. Achmad Yani II (Jl. Pemuda) No. 3, RT 03, Samarinda', 'Samarinda', '0541 - 202208', '', '', 0, 25, 4, 'KALTIM', 'SSMPMD', '', '', 0, 0, ''),
('SSM PONTIANAK', 'JL. GAJAH MADA NO.149 A-B, PONTIANAK', 'Pontianak', '0561-762669 / 0561-5', '', '', 0, 20, 3, 'KALBAR', 'SSMPTK', '', '', 0, 0, ''),
('SSM SAMPIT', 'JL. HM ARSYAD RT 39 / RW 7 KOTA SAMPIT KALIMANTAN TENGAH', 'Sampit', '81257575995', '', '', 0, 16, 8, 'KALSEL', 'SSMSPT', '', '', 0, 0, ''),
('SSM SANGATTA', 'JL. YOS SUDARSO II NO. 3, SANGATTA', 'Sangatta', '0549 - 23203', '', '', 0, 26, 4, 'KALTIM', 'SSMSGT', '', '', 0, 0, ''),
('SSM SANGGAU', 'JL. JENDRAL AHMAD YANI NO. 16', 'Sanggau', '0564-2025098', '', '', 0, 19, 3, 'KALBAR', 'SSMSGU', '', '', 0, 0, ''),
('SSM SINTANG', 'JL. LINTAS MELAWI, SINTANG', 'Sintang', '0565 - 23617', '', '', 0, 18, 3, 'KALBAR', 'SSMSTG', '', '', 0, 0, ''),
('SSM SUTOYO', 'JL. SOETOYO RT.02, KOMP PERTOKOAN HARMONI, BANJARMASIN \r\n', 'Banjarmasin', '0511-3362480', '', '', 0, 13, 8, 'KALSEL', 'SSMSTY', '', '', 0, 0, ''),
('SSM TANAH GROGOT', 'JL. RM. NOTO SUNARDI II NO.51, RT.14, TANAH GROGOT', 'Tanah Grogot', '0554 - 2027752', '', '', 0, 27, 4, 'KALTIM', 'SSMGRT', '', '', 0, 0, ''),
('SSM TANGERANG', 'Jl. Imam Bonjol Ruko 100 D-E-F, Karawaci - Tangerang', 'Tangerang', '021 - 55730781', '', '', 0, 17, 2, 'JKT', 'SSMTNG', '', '', 0, 0, ''),
('SSM TARAKAN', 'JL. SUDIRMAN RT 02, TARAKAN', 'Tarakan', '0551 - 51690', '', '', 0, 29, 4, 'KALTIM', 'SSMTRK', '', '', 0, 0, ''),
('SSM TENGGARONG', 'JL. KH AHMAD MUKSIN RT.02, TENGGARONG', 'Tenggarong', '0541 - 662428', '', '', 0, 28, 4, 'KALTIM', 'SSMTGR', '', '', 0, 0, '');

-- --------------------------------------------------------

--
-- Struktur dari tabel `mst_quest`
--

CREATE TABLE `mst_quest` (
  `id_quest` int(11) NOT NULL,
  `no_type` int(11) DEFAULT NULL,
  `type` text NOT NULL,
  `quest` varchar(100) NOT NULL,
  `nm_quest` varchar(500) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `mst_quest`
--

INSERT INTO `mst_quest` (`id_quest`, `no_type`, `type`, `quest`, `nm_quest`) VALUES
(1, 1, 'SERVICE', 'Service Counter', 'Apakah SERVICE COUNTER memberikan pelayanan yang baik ketika service?'),
(2, 2, 'SERVICE', 'Hasil Service', 'Apakah kualitas hasil service motor anda memuaskan?'),
(3, 3, 'SERVICE', 'Kondisi Showroom dan Bengkel', 'Apakah showroom dan bengkel Super Sukses Motor cukup nyaman/memadai?'),
(4, 4, 'SERVICE', 'Merekomendasikan SSM', 'Apakah anda akan merekomendasikan service motor Kawasaki di bengkel Super Sukses Motor?'),
(5, 1, 'SALES', 'TIM SALES', 'Apakah TIM SALES memberikan pelayanan yang baik dalam proses penjualan?'),
(6, 2, 'SALES', 'Kondisi Motor', 'Apakah motor Kawasaki anda diterima dalam kondisi baik?'),
(7, 3, 'SALES', 'Kondisi Showroom', 'Apakah showroom Super Sukses Motor cukup nyaman/memadai?'),
(8, 4, 'SALES', 'Merekomendasikan SSM', 'Apakah bapak/ibu akan merekomendasikan pembelian motor Kawasaki di Super Sukses Motor kepada orang lain?');

-- --------------------------------------------------------

--
-- Struktur dari tabel `mst_respon`
--

CREATE TABLE `mst_respon` (
  `id_respon` int(11) NOT NULL,
  `nm_respon` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `mst_respon`
--

INSERT INTO `mst_respon` (`id_respon`, `nm_respon`) VALUES
(1, 'Tidak Aktif'),
(2, 'Salah Sambung'),
(3, 'Tidak Beri Nilai'),
(4, 'Tidak Terdaftar'),
(5, 'Tidak Direspon'),
(6, 'Call Sukses');

-- --------------------------------------------------------

--
-- Struktur dari tabel `tb_detsurv`
--

CREATE TABLE `tb_detsurv` (
  `id_detsurv` int(11) NOT NULL,
  `id_hedsurv` int(11) DEFAULT NULL,
  `id_surv` int(11) DEFAULT NULL,
  `id_quest` int(11) DEFAULT NULL,
  `nilai` varchar(20) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Struktur dari tabel `tb_hedsurv`
--

CREATE TABLE `tb_hedsurv` (
  `id_hedsurv` int(11) NOT NULL,
  `id_surv` int(11) DEFAULT NULL,
  `tgl_telp` varchar(20) DEFAULT NULL,
  `time_telp` varchar(20) DEFAULT NULL,
  `id_respon` int(11) DEFAULT NULL,
  `kritik_saran` text DEFAULT NULL,
  `harga_beli` text DEFAULT NULL,
  `harga_diskon` text DEFAULT NULL,
  `status_dapat_invoice` int(11) DEFAULT NULL,
  `tipe_cust` int(11) DEFAULT NULL,
  `status_insert_detsurv` int(11) DEFAULT NULL,
  `created_at_by` text DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Struktur dari tabel `tb_surv`
--

CREATE TABLE `tb_surv` (
  `id_surv` int(11) NOT NULL,
  `tgl_surv` date DEFAULT NULL,
  `id_comp` int(11) NOT NULL,
  `id_cust` int(11) NOT NULL,
  `nama_cust` varchar(100) NOT NULL,
  `telepon1` varchar(13) DEFAULT NULL,
  `telepon2` varchar(13) DEFAULT NULL,
  `hp` varchar(13) DEFAULT NULL,
  `type` varchar(100) NOT NULL,
  `tgl_inv` date NOT NULL,
  `tgl_telp` varchar(20) DEFAULT NULL,
  `time_telp` varchar(20) DEFAULT NULL,
  `diskon` int(11) NOT NULL,
  `id_respon` int(11) DEFAULT NULL,
  `kritik_saran` text DEFAULT NULL,
  `harga_beli` text DEFAULT NULL,
  `harga_diskon` text DEFAULT NULL,
  `status_dapat_invoice` int(11) DEFAULT NULL,
  `tipe_cust` int(1) NOT NULL,
  `tgl_wo` date DEFAULT NULL,
  `tlp_wo` varchar(13) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Struktur dari tabel `tb_surv_detail_telepon`
--

CREATE TABLE `tb_surv_detail_telepon` (
  `id_detail_telepon` int(11) NOT NULL,
  `id_header_telepon` int(11) DEFAULT NULL,
  `telepon` varchar(25) DEFAULT NULL,
  `id_respon` int(11) DEFAULT NULL,
  `tgl_detail_telepon` varchar(20) DEFAULT NULL,
  `time_detail_telepon` varchar(20) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Struktur dari tabel `tb_surv_header_telepon`
--

CREATE TABLE `tb_surv_header_telepon` (
  `id_header_telepon` int(11) NOT NULL,
  `id_surv` int(11) DEFAULT NULL,
  `flex_telepon` varchar(25) DEFAULT NULL,
  `counter` int(11) DEFAULT NULL,
  `tipe_cust` int(11) DEFAULT NULL,
  `created_at_by` text DEFAULT NULL,
  `updated_at_by` text DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Indexes for dumped tables
--

--
-- Indeks untuk tabel `mst_company`
--
ALTER TABLE `mst_company`
  ADD PRIMARY KEY (`nama`);

--
-- Indeks untuk tabel `mst_quest`
--
ALTER TABLE `mst_quest`
  ADD PRIMARY KEY (`id_quest`);

--
-- Indeks untuk tabel `mst_respon`
--
ALTER TABLE `mst_respon`
  ADD PRIMARY KEY (`id_respon`);

--
-- Indeks untuk tabel `tb_detsurv`
--
ALTER TABLE `tb_detsurv`
  ADD PRIMARY KEY (`id_detsurv`);

--
-- Indeks untuk tabel `tb_hedsurv`
--
ALTER TABLE `tb_hedsurv`
  ADD PRIMARY KEY (`id_hedsurv`);

--
-- Indeks untuk tabel `tb_surv`
--
ALTER TABLE `tb_surv`
  ADD PRIMARY KEY (`id_surv`);

--
-- Indeks untuk tabel `tb_surv_detail_telepon`
--
ALTER TABLE `tb_surv_detail_telepon`
  ADD PRIMARY KEY (`id_detail_telepon`);

--
-- Indeks untuk tabel `tb_surv_header_telepon`
--
ALTER TABLE `tb_surv_header_telepon`
  ADD PRIMARY KEY (`id_header_telepon`);

--
-- AUTO_INCREMENT untuk tabel yang dibuang
--

--
-- AUTO_INCREMENT untuk tabel `mst_quest`
--
ALTER TABLE `mst_quest`
  MODIFY `id_quest` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT untuk tabel `mst_respon`
--
ALTER TABLE `mst_respon`
  MODIFY `id_respon` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT untuk tabel `tb_detsurv`
--
ALTER TABLE `tb_detsurv`
  MODIFY `id_detsurv` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT untuk tabel `tb_hedsurv`
--
ALTER TABLE `tb_hedsurv`
  MODIFY `id_hedsurv` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT untuk tabel `tb_surv`
--
ALTER TABLE `tb_surv`
  MODIFY `id_surv` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT untuk tabel `tb_surv_detail_telepon`
--
ALTER TABLE `tb_surv_detail_telepon`
  MODIFY `id_detail_telepon` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT untuk tabel `tb_surv_header_telepon`
--
ALTER TABLE `tb_surv_header_telepon`
  MODIFY `id_header_telepon` int(11) NOT NULL AUTO_INCREMENT;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
